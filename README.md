# Timeplayer-9000 Custom Load Pages

The [Timeplayer-9000](https://github.com/borevitzlab/timeplayer-9000) was developed for initial deployment on the [Traitcapture](https://traitcapture.org/timestreams) website for viewing of fixed records. This project holds several pages in use for broader application of the Timeplayer with loading from Grafana frontends and easier interfaces to expose the full functionality of the Timeplayer.

## Notes on usage with Grafana

Grafana Angular template for linking to `amrf_get.html`
```html
<div id="last-site-img" style="display: {{response[0] ? 'block' : 'none'}};">
<a  
  title="720x - {{response[0].rows[0][1]}}"
  target="_blank" 
  href="https://timeplayer.amrf.org.au/?org={{response[0].rows[0][3]}}&camname={{response[0].rows[0][2]}}&from={{ctrl.range.from.format('X')}}&to={{ctrl.range.to.format('X')}}&defaultImgTs={{response[0].rows[0][0]}}">
  <img
     title="720x - {{response[0].rows[0][1]}}"
     src="https://cdn.amrf.org.au/d/cameras/{{response[0].rows[0][3]}}/{{response[0].rows[0][2]}}/{{response[0].rows[0][2]}}~720x/$dirstamp/{{response[0].rows[0][2]}}~720x_$filestamp.jpg?timestamp_ms={{response[0].rows[0][0]}}&interval_sec=1" 
     style='display:block;margin:auto;width: 100%;max-height: 100%;object-fit: contain;'/>
</a>
</div>
<div id="no-last-site-img" style="display: {{response[0] ? 'none' : 'block'}};">
  No images currently available for given time period.
</div>
```

Template notes:
- Used with query `SELECT time,file_bytes,camera_name,org_name FROM image_metrics WHERE $timeFilter AND camera_name =~ /^$camera_name$/  ORDER BY time DESC LIMIT 1`
- `{{ctrl.range.from.format('X')}}` = Unix timestamp in seconds (lowercase `'x'` format for milliseconds) (replace `from` with `to` as needed for the other end of the dashboard range)
- `{{response[0].rows[0][3]}}` = org name
- `{{response[0].rows[0][2]}}` = camera name
- `style="display: {{response[0] ? 'none' : 'block'}};"` used to hide/unhide the image holder and "no image" text depending on whether an image record exists.
- Angular to be deprecated in Grafana - to update when Grafana version is updated.

Previous Angular template, for reference:
```html
720x - {{response[0].rows[0][1]}}
<a  
  title="720x - {{response[0].rows[0][1]}}"
  target="_blank" 
  href="https://img.amrf.org.au/cameras/{{response[0].rows[0][3]}}/{{response[0].rows[0][2]}}/{{response[0].rows[0][2]}}~720x/$dirstamp/{{response[0].rows[0][2]}}~720x_$filestamp.jpg?timestamp_ms={{response[0].rows[0][0]}}&interval_sec=1">
  <img
     title="720x - {{response[0].rows[0][1]}}"
     src="https://cdn.amrf.org.au/d/cameras/{{response[0].rows[0][3]}}/{{response[0].rows[0][2]}}/{{response[0].rows[0][2]}}~720x/$dirstamp/{{response[0].rows[0][2]}}~720x_$filestamp.jpg?timestamp_ms={{response[0].rows[0][0]}}&interval_sec=1" 
     style='display:block;margin:auto;width: 100%;max-height: 100%;object-fit: contain;'/>
</a>
```

Old Angular template directly converted to use the timeplayer-loader:
```html
720x - {{response[0].rows[0][1]}}
<a  
  title="720x - {{response[0].rows[0][1]}}"
  target="_blank" 
  href="https://loader.traitcapture.org/amrf_get.html?org={{response[0].rows[0][3]}}&camname={{response[0].rows[0][2]}}&from={{ctrl.range.from.format('X')}}&to={{ctrl.range.to.format('X')}}&defaultImgTs={{response[0].rows[0][0]}}">
  <img
     title="720x - {{response[0].rows[0][1]}}"
     src="https://cdn.amrf.org.au/d/cameras/{{response[0].rows[0][3]}}/{{response[0].rows[0][2]}}/{{response[0].rows[0][2]}}~720x/$dirstamp/{{response[0].rows[0][2]}}~720x_$filestamp.jpg?timestamp_ms={{response[0].rows[0][0]}}&interval_sec=1" 
     style='display:block;margin:auto;width: 100%;max-height: 100%;object-fit: contain;'/>
</a>
```
